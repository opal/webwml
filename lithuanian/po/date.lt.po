#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2009-02-22 14:34+0300\n"
"Last-Translator: Aleksandr Charkov <alcharkov@gmail.com>\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. List of weekday names (used in modification dates)
#: ../../english/template/debian/ctime.wml:11
msgid "Sun"
msgstr "Sekmadienis"

#: ../../english/template/debian/ctime.wml:12
msgid "Mon"
msgstr "Pirmadienis"

#: ../../english/template/debian/ctime.wml:13
msgid "Tue"
msgstr "Antradienis"

#: ../../english/template/debian/ctime.wml:14
msgid "Wed"
msgstr "Trečiadienis"

#: ../../english/template/debian/ctime.wml:15
msgid "Thu"
msgstr "Ketvirtadienis"

#: ../../english/template/debian/ctime.wml:16
msgid "Fri"
msgstr "Penktadienis"

#: ../../english/template/debian/ctime.wml:17
msgid "Sat"
msgstr "Šeštadienis"

#. List of month names (used in modification dates, and may be used in news 
#. listings)
#: ../../english/template/debian/ctime.wml:23
msgid "Jan"
msgstr "1"

#: ../../english/template/debian/ctime.wml:24
msgid "Feb"
msgstr "2"

#: ../../english/template/debian/ctime.wml:25
msgid "Mar"
msgstr "3"

#: ../../english/template/debian/ctime.wml:26
msgid "Apr"
msgstr "4"

#: ../../english/template/debian/ctime.wml:27
msgid "May"
msgstr "5"

#: ../../english/template/debian/ctime.wml:28
msgid "Jun"
msgstr "6"

#: ../../english/template/debian/ctime.wml:29
msgid "Jul"
msgstr "7"

#: ../../english/template/debian/ctime.wml:30
msgid "Aug"
msgstr "8"

#: ../../english/template/debian/ctime.wml:31
msgid "Sep"
msgstr "9"

#: ../../english/template/debian/ctime.wml:32
msgid "Oct"
msgstr "10"

#: ../../english/template/debian/ctime.wml:33
msgid "Nov"
msgstr "11"

#: ../../english/template/debian/ctime.wml:34
msgid "Dec"
msgstr "12"

#. List of long month names (may be used in "spoken" dates and date ranges).
#: ../../english/template/debian/ctime.wml:39
msgid "January"
msgstr "Sausis"

#: ../../english/template/debian/ctime.wml:40
msgid "February"
msgstr "Vasaris"

#: ../../english/template/debian/ctime.wml:41
msgid "March"
msgstr "Kovas"

#: ../../english/template/debian/ctime.wml:42
msgid "April"
msgstr "Balandis"

#. The <void> tag is to distinguish short and long forms of May.
#. Do not put it in msgstr.
#: ../../english/template/debian/ctime.wml:45
msgid "<void id=\"fullname\" />May"
msgstr "<void id=\"fullname\" />Gegužė"

#: ../../english/template/debian/ctime.wml:46
msgid "June"
msgstr "Birželis"

#: ../../english/template/debian/ctime.wml:47
msgid "July"
msgstr "Liepa"

#: ../../english/template/debian/ctime.wml:48
msgid "August"
msgstr "Rugpjūtis"

#: ../../english/template/debian/ctime.wml:49
msgid "September"
msgstr "Rugsėjis"

#: ../../english/template/debian/ctime.wml:50
msgid "October"
msgstr "Spalis"

#: ../../english/template/debian/ctime.wml:51
msgid "November"
msgstr "Lapkritis"

#: ../../english/template/debian/ctime.wml:52
msgid "December"
msgstr "Gruodis"

#. $dateform: Date format (sprintf) for modification dates.
#. Available variables are: $mday = day-of-month, $monnr = month number,
#. $mon = month string (from @moy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:60
msgid ""
"q{[%]s, [%]s [%]2d [%]02d:[%]02d:[%]02d [%]s [%]04d}, $wday, $mon, $mday, "
"$hour, $min, $sec, q{UTC}, 1900+$year"
msgstr ""
"q{[%]s [%]04d-[%]02d-[%]02d [%]02d.[%]02d.[%]02d [%]s}, $wday, 1900+$year, "
"$monnr+1, $mday, $hour, $min, $sec, q{UTC}"

#. $newsdateform: Date format (sprintf) for news items.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @moy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:68
msgid "q{[%]02d [%]s [%]04d}, $mday, $mon_str, $year"
msgstr "q{[%]4d-[%]02d-[%]02d}, $year, $mon, $mday"

#. $spokendateform: Date format (sprintf) for "spoken" dates
#. (such as the current release date).
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @longmoy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:79
msgid "q{[%]02d [%]s [%]d}, $mday, $mon_str, $year"
msgstr "q{[%]02d [%]s [%]d}, $mday, $mon_str, $year"

#. $spokendateform_noyear: Date format (sprintf) for "spoken" dates
#. (such as the current release date), without the year.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @longmoy).
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:90
msgid "q{[%]d [%]s}, $mday, $mon_str"
msgstr "q{[%]d [%]s}, $mday, $mon_str"

#. $spokendateform_noday: Date format (sprintf) for "spoken" dates
#. (such a conference event), without the day.
#. Available variables are: $mon = month number,
#. $mon_str = month string (from @longmoy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:99
#, fuzzy
msgid "q{[%]s [%]s}, $mon_str, $year"
msgstr "q{[%]02d [%]s [%]d}, $mday, $mon_str, $year"

#. $rangeform_samemonth: Date format (sprintf) for date ranges
#. (used mainly for events pages), for ranges within the same month.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $sday = start day-of-month, $eday = end
#. day-of-month, $smon = month number, $smon_str = month string (from @longmoy)
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:110
msgid "q{[%]d-[%]d [%]s}, $sday, $eday, $smon_str"
msgstr "q{[%]d-[%]d [%]s}, $sday, $eday, $smon_str"

#. $rangeform_severalmonths: Date format (sprintf) for date ranges
#. (used mainly for events pages), for ranges spanning the end of a month.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $sday = start day-of-month, $eday, end
#. day-of-month, $smon = start month number, $emon = end month number,
#. $smon_str = start month string (from @longmoy), $emon_str = end month string
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:122
msgid "q{[%]d [%]s-[%]d [%]s}, $sday, $smon_str, $eday, $emon_str"
msgstr "q{[%]d [%]s-[%]d [%]s}, $sday, $smon_str, $eday, $emon_str"
