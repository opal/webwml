#use wml::debian::translation-check translation="e0c304b76f11a690aabf7b1f4730579e7e925674" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>La mise à jour de sécurité pour OpenSSH publiée dans la DLA 1500-1
introduisait un bogue dans openssh-client : lorsque le réacheminement d’X11 est
activé (à l’aide d’une configuration pour tout le système dans ssh_config ou à
l’aide d’une option -X de ligne de commande), mais qu’aucun DISPLAY n’est
défini, le client produit un avertissement « DISPLAY (null) invalid; disabling
X11 forwarding ». Ce bogue a été introduit par l’ensemble de correctifs pour
le problème
<a href="https://security-tracker.debian.org/tracker/CVE-2016-1908">CVE-2016-1908</a>.
À titre indicatif, ce qui suit est la partie concernée de la publication
originale.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1908">CVE-2016-1908</a>

<p>OpenSSH gérait incorrectement les redirections X11 non authentifiées lorsqu’un
serveur X désactivait l’extension SECURITY. Des connexions non authentifiées
pourraient obtenir les droits de redirection X11 authentifiée. Signalé par
Thomas Hoger.</p></li>
</ul>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 1:6.7p1-5+deb8u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssh.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1500-2.data"
# $Id: $
