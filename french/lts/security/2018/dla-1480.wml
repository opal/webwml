#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Ruby 2.1.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2337">CVE-2016-2337</a>

<p>Une confusion de type existe dans la méthode de classe _cancel_eval de
TclTkIp de Ruby. Un attaquant passant un type différent d’objet de String comme
argument <q>retval</q> peut causer l’exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000073">CVE-2018-1000073</a>

<p>RubyGems contient une vulnérabilité de traversée de répertoires dans la
fonction install_location de package.rb qui pourrait aboutir à une traversée de
répertoires lors de l’écriture dans un basedir lié symboliquement, en dehors de
la racine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000074">CVE-2018-1000074</a>

<p>RubyGems contient une vulnérabilité de désérialisation de données non fiables
dans la commande owner qui peut aboutir à l’exécution de code. Cette attaque
semble être exploitable en forçant la victime à exécuter la commande
<q>gem owner</q> sur un gem avec un fichier YAML contrefait pour l’occasion.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.1.5-2+deb8u5.</p>
<p>Nous vous recommandons de mettre à jour vos paquets ruby2.1.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1480.data"
# $Id: $
