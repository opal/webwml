<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been found in the Apache HTTPD server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3167">CVE-2017-3167</a>

    <p>Emmanuel Dreyfus reported that the use of ap_get_basic_auth_pw() by
    third-party modules outside of the authentication phase may lead to
    authentication requirements being bypassed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3169">CVE-2017-3169</a>

    <p>Vasileios Panopoulos of AdNovum Informatik AG discovered that
    mod_ssl may dereference a NULL pointer when third-party modules call
    ap_hook_process_connection() during an HTTP request to an HTTPS port
    leading to a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7668">CVE-2017-7668</a>

    <p>Javier Jimenez reported that the HTTP strict parsing contains a flaw
    leading to a buffer overread in ap_find_token(). A remote attacker
    can take advantage of this flaw by carefully crafting a sequence of
    request headers to cause a segmentation fault, or to force
    ap_find_token() to return an incorrect value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7679">CVE-2017-7679</a>

    <p>ChenQin and Hanno Boeck reported that mod_mime can read one byte
    past the end of a buffer when sending a malicious Content-Type
    response header.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.2.22-13+deb7u9.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1009.data"
# $Id: $
