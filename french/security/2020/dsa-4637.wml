#use wml::debian::translation-check translation="44280ae920ca8f6697eb3d2e55b736bd1753e429" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Kobus van Schoor a découvert que network-manager-ssh, un greffon pour
fournir l'intégration de VPN pour SSH dans NetworkManager, est prédisposé à
une vulnérabilité d'élévation de privilèges. Un utilisateur local doté des
droits pour modifier une connexion peut tirer avantage de ce défaut pour
exécuter des commandes arbitraires en tant que superutilisateur.</p>

<p>Cette mise à jour supprime la prise en charge du passage d'options
supplémentaires de SSH à l'invocation de ssh.</p>

<p>Pour la distribution oldstable (Stretch), ce problème a été corrigé dans
la version 1.2.1-1+deb9u1.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 1.2.10-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets network-manager-ssh.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de network-manager-ssh,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/network-manager-ssh">\
https://security-tracker.debian.org/tracker/network-manager-ssh</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4637.data"
# $Id: $
