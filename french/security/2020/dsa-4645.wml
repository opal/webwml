#use wml::debian::translation-check translation="9f255d7246e224b0653339ff32337f4de3f80b46" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le navigateur web
Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20503">CVE-2019-20503</a>

 <p>Natalie Silvanovich a découvert un problème de lecture hors limites
dans la bibliothèque usrsctp.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6422">CVE-2020-6422</a>

<p>David Manouchehri a découvert un problème d'utilisation de mémoire après
libération dans l'implémentation de WebGL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6424">CVE-2020-6424</a>

<p>Sergei Glazunov a découvert un problème d'utilisation de mémoire après
libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6425">CVE-2020-6425</a>

<p>Sergei Glazunov a découvert une erreur d'application de politique liée
aux extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6426">CVE-2020-6426</a>

<p>Avihay Cohen a découvert une erreur d'implémentation dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6427">CVE-2020-6427</a>

<p>Man Yue Mo a découvert un problème d'utilisation de mémoire après
libération dans l'implémentation audio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6428">CVE-2020-6428</a>

<p>Man Yue Mo a découvert un problème d'utilisation de mémoire après
libération dans l'implémentation audio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6429">CVE-2020-6429</a>

<p>Man Yue Mo a découvert un problème d'utilisation de mémoire après
libération dans l'implémentation audio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6449">CVE-2020-6449</a>

<p>Man Yue Mo a découvert un problème d'utilisation de mémoire après
libération dans l'implémentation audio.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), le suivi de sécurité du paquet
Chromium a été arrêté.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 80.0.3987.149-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets chromium.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de chromium, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4645.data"
# $Id: $
