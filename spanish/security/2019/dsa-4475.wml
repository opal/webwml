#use wml::debian::translation-check translation="aeeaf339a8d997a10158499aedcffe4ec71a7d78"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Joran Dirk Greef descubrió que valores de un solo uso («nonces») excesivamente grandes usados con
ChaCha20-Poly1305 se procesaban incorrectamente, lo que podía dar lugar a que se
reutilizaran. Esto no afecta a usos de ChaCha20-Poly1305 internos al propio OpenSSL,
como, por ejemplo, a TLS.</p>

<p>Para la distribución «estable» (stretch), este problema se ha corregido en
la versión 1.1.0k-1~deb9u1. Este DSA actualiza también openssl1.0 (que
no está afectado por <a href="https://security-tracker.debian.org/tracker/CVE-2019-1543">\
CVE-2019-1543</a>) a 1.0.2s-1~deb9u1.</p>

<p>Le recomendamos que actualice los paquetes de openssl.</p>

<p>Para información detallada sobre el estado de seguridad de openssl, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4475.data"
