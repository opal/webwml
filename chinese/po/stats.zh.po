msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Boyuan Yang <073plan@gmail.com>\n"
"Language-Team: \n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../stattrans.pl:278 ../../stattrans.pl:494
msgid "Wrong translation version"
msgstr "譯文版本錯誤"

#: ../../stattrans.pl:280
msgid "This translation is too out of date"
msgstr "此譯文太過時了"

#: ../../stattrans.pl:282
msgid "The original is newer than this translation"
msgstr "原文比此譯文新"

#: ../../stattrans.pl:286 ../../stattrans.pl:494
msgid "The original no longer exists"
msgstr "原文不再存在"

#: ../../stattrans.pl:470
msgid "hits"
msgstr "命中"

#: ../../stattrans.pl:470
msgid "hit count N/A"
msgstr "命中计数 N/A"

#: ../../stattrans.pl:488 ../../stattrans.pl:489
msgid "Click to fetch diffstat data"
msgstr "点击以获取 diffstat 数据"

#: ../../stattrans.pl:599 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr "使用 <transstatslink> 创建"

#: ../../stattrans.pl:604
msgid "Translation summary for"
msgstr "翻譯概要 for"

#: ../../stattrans.pl:607
msgid "Translated"
msgstr "已翻譯"

#: ../../stattrans.pl:607 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "最新"

#: ../../stattrans.pl:607 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "過時"

#: ../../stattrans.pl:607 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "未翻譯"

#: ../../stattrans.pl:608 ../../stattrans.pl:609 ../../stattrans.pl:610
#: ../../stattrans.pl:611
msgid "files"
msgstr "個[CN:文件:][HKTW:檔案:]"

#: ../../stattrans.pl:614 ../../stattrans.pl:615 ../../stattrans.pl:616
#: ../../stattrans.pl:617
msgid "bytes"
msgstr "[CN:字節:][HKTW:位元組:]"

#: ../../stattrans.pl:624
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"注意：页面列表使用其访问次数进行排序。在页面名称上悬停鼠标光标可以查看访问命"
"中次数。"

#: ../../stattrans.pl:630
msgid "Outdated translations"
msgstr "過時的翻譯"

#: ../../stattrans.pl:632 ../../stattrans.pl:686
msgid "File"
msgstr "[CN:文件:][HKTW:檔案:]"

#: ../../stattrans.pl:634
msgid "Diff"
msgstr "差异"

#: ../../stattrans.pl:636
msgid "Comment"
msgstr "備註"

#: ../../stattrans.pl:637
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr "Git 命令行"

#: ../../stattrans.pl:640
msgid "Log"
msgstr "日誌"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "譯文"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "維護者"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "狀態"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "翻譯者"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "日期"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "未翻譯的一般頁面"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "未翻譯的一般頁面"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "未翻譯的新聞"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "未翻譯的新聞"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "未翻譯的顧問/[CN:用戶:][HKTW:使用者:]頁面"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "未翻譯的顧問/[CN:用戶:][HKTW:使用者:]頁面"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "未翻譯的國際頁面"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "未翻譯的國際頁面"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "已翻譯頁面 (最新)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "已翻譯模板 (PO [CN:文件:][HKTW:檔案:])"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "PO 翻譯統計"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "模糊"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "未翻譯"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "總數"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "總數："

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "已翻譯網頁"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "翻譯統計，按頁面數量"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "語言"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "翻譯"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "已翻譯網頁（按[CN:文件:][HKTW:檔案:]大小）"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "翻譯統計，按頁面大小"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Debian 網站翻譯統計"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "一共有 %d 個頁面需要翻譯。"

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "一共有 %d [CN:字節:][HKTW:位元组:]需要翻譯。"

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "一共有 %d 個[CN:字符串:][HKTW:字串:]需要翻譯。"

#~ msgid "Unified diff"
#~ msgstr "合并差异"

#~ msgid "Colored diff"
#~ msgstr "带颜色差异"

#~ msgid "Commit diff"
#~ msgstr "提交差异"

#~ msgid "Created with"
#~ msgstr "此網頁創建工具："

#~ msgid "Origin"
#~ msgstr "原文"
