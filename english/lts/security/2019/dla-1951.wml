<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a denial of service vulnerability
in the libtomcrypt cryptographic library.</p>

<p>An out-of-bounds read and crash could occur via carefully-crafted
"DER" encoded data (eg. by importing an X.509 certificate).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17362">CVE-2019-17362</a>

    <p>In LibTomCrypt through 1.18.2, the der_decode_utf8_string function (in der_decode_utf8_string.c) does not properly detect certain invalid UTF-8 sequences. This allows context-dependent attackers to cause a denial of service (out-of-bounds read and crash) or read information from other memory locations via carefully crafted DER-encoded data.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.17-6+deb8u1.</p>

<p>We recommend that you upgrade your libtomcrypt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1951.data"
# $Id: $
