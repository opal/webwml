<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were found in QEMU, a fast processor emulator:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12617">CVE-2018-12617</a>

    <p>The qmp_guest_file_read function (qga/commands-posix.c) is affected
    by an integer overflow and subsequent memory allocation failure. This
    weakness might be leveraged by remote attackers to cause denial of
    service (application crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16872">CVE-2018-16872</a>

    <p>The usb_mtp_get_object, usb_mtp_get_partial_object and
    usb_mtp_object_readdir functions (hw/usb/dev-mtp.c) are affected by a
    symlink attack. Remote attackers might leverage this vulnerability to
    perform information disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6778">CVE-2019-6778</a>

    <p>The tcp_emu function (slirp/tcp_subr.c) is affected by a heap buffer
    overflow caused by insufficient validation of available space in the
    sc_rcv->sb_data buffer. Remote attackers might leverage this flaw to
    cause denial of service, or any other unspecified impact.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:2.1+dfsg-12+deb8u10.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1694.data"
# $Id: $
