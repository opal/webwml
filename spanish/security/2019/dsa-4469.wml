#use wml::debian::translation-check translation="2c3a538ea007bd4c5a40618a3303a4fdd1c34f42"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron dos vulnerabilidades en Libvirt, una biblioteca
de abstracción de virtualización, que permitían que un cliente de las API con permisos de solo lectura
ejecutara órdenes arbitrarias a través de la API virConnectGetDomainCapabilities
o leyera o ejecutara ficheros arbitrarios a través de la
API virDomainSaveImageGetXMLDesc.</p>

<p>Adicionalmente, se actualizó el conjunto de características de las cpu en libvirt para hacer más sencilla la corrección de
<a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">\
CVE-2018-3639</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2017-5753">\
CVE-2017-5753</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2017-5715">\
CVE-2017-5715</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-12126">\
CVE-2018-12126</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-12127">\
CVE-2018-12127</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-12130">\
CVE-2018-12130</a> y <a href="https://security-tracker.debian.org/tracker/CVE-2019-11091">\
CVE-2019-11091</a> añadiendo soporte para las funcionalidades de CPU md-clear,
ssbd, spec-ctrl e ibpb al escoger modelos de CPU, sin necesidad de
trasladárselas al anfitrión.</p>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 3.0.0-4+deb9u4.</p>

<p>Le recomendamos que actualice los paquetes de libvirt.</p>

<p>Para información detallada sobre el estado de seguridad de libvirt, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/libvirt">\
https://security-tracker.debian.org/tracker/libvirt</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4469.data"
