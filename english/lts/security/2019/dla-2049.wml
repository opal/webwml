<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in imagemagick, an image processing
toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19948">CVE-2019-19948</a>

    <p>Heap-buffer-overflow in WriteSGIImage (coders/sgi.c) caused by insufficient
    validation of row and column sizes. This vulnerability might be leveraged by
    remote attackers to cause denial of service or any other unspecified impact
    via crafted image data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19949">CVE-2019-19949</a>

    <p>Heap-based buffer over-read (off-by-one) in WritePNGImage (coders/png.c)
    caused by missing length check prior pointer dereference. This vulnerability
    might be leveraged by remote attackers to cause denial of service via
    crafted image data.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
8:6.8.9.9-5+deb8u19.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2049.data"
# $Id: $
