<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The libarchive-zip-perl package is vulnerable to a directory traversal
attack in Archive::Zip. It was found that the Archive::Zip module did
not properly sanitize paths while extracting zip files. An attacker
able to provide a specially crafted archive for processing could use
this flaw to write or overwrite arbitrary files in the context of the
Perl interpreter.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.39-1+deb8u1.</p>

<p>We recommend that you upgrade your libarchive-zip-perl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1440.data"
# $Id: $
